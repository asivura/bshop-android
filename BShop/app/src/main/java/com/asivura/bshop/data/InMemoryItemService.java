package com.asivura.bshop.data;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.asivura.bshop.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

/**
 * Created by asivura on 01.02.15.
 */
public class InMemoryItemService implements ItemService{
    private final Context context;
    private final ItemStorage itemStorage;
    private List<Item> items;
    private List<WeakReference<ItemService.Listener>> listeners;


    public InMemoryItemService(Context context, ItemStorage itemStorage) {
        this.itemStorage = itemStorage;
        this.context = context;
        listeners = new LinkedList<>();

        try {
            items = itemStorage.read();
            Log.d("TAG", items.toString());
        } catch (IOException e) {
            throw new RuntimeException("Can not read items", e);
        }

    }

    @Override
    public List<Item> getAll() {
        return items;

    }

    @Override
    public Item get(String id) {
        int index = indexOf(id);
        if (index == -1) {
            throw new RuntimeException("Item with id " + id + " does not exist");
        }
        return items.get(index);
    }

    private int indexOf(String id) {
        for (int i= 0; i< items.size(); i++) {
            if (items.get(i).getId().equals(id)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void asyncSave(final Item item, final Integer quantityInc, final OperationListener listener) {
        new AsyncTask<Void,Void,Exception>() {

            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    //Simulating background work
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    return e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(Exception e) {
                if (e != null) {
                    listener.onError(context, e);
                }
                if (save(item, quantityInc)) {
                    listener.onCompleted(context);
                } else {
                    listener.onError(context, new Exception("Save failed"));
                }


            }
        }.execute();
    }

    @Override
    public void asyncBuy(final Item item, final OperationListener listener) {
        new AsyncTask<Void,Void,Exception>() {
            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    //Simulating background work
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    return e;
                }

                return null;
            }

            @Override
            protected void onPostExecute(Exception e) {
                if (e != null) {
                    listener.onError(context, e);
                }
                if (buy(item)) {
                    listener.onCompleted(context);
                } else {
                    listener.onError(context, new Exception("Save failed"));
                }


            }
        }.execute();
    }

    @Override
    public Item create() {
        InMemoryItem item = new InMemoryItem();
        item.id = UUID.randomUUID().toString();
        return item;
    }

    private Boolean save(Item newItem, Integer quantityInc) {
        int index = items.indexOf(newItem);

        if (index == -1) {
            newItem.setQuantity(newItem.getQuantity() + quantityInc);
            items.add(newItem);
            notifyItemAdded(newItem);
        } else {
            Item item = items.get(index);
            item.setTitle(newItem.getTitle());
            item.setPrice(newItem.getPrice());

            item.setQuantity(item.getQuantity() + quantityInc);
            saveToStorage();
            notifyItemChanged(item);
        }
        return true;
    }

    @Override
    public void addListener(Listener listener) {
        listeners.add(new WeakReference<>(listener));

    }

    @Override
    public void removeListener(Listener removedListener) {
        Iterator<WeakReference<Listener>> iterator = listeners.iterator();
        while (iterator.hasNext()) {
            Listener listener = iterator.next().get();
            if (listener.equals(removedListener)) {
                iterator.remove();
            }
        }
    }

    public void saveToStorage() {
        try {
            itemStorage.write(items);
        } catch (IOException e) {
            throw new RuntimeException("Save to storage failed");
        }
    }

    private Boolean buy(Item item) {

        int index = items.indexOf(item);

        if (index == -1) {
            throw new RuntimeException("Item with id " + item.getId() + " does not exist");
        }

        Item currentItem = items.get(index);
        if (currentItem.getQuantity() >= 1) {
            currentItem.setQuantity(item.getQuantity() - 1);
            notifyItemChanged(currentItem);
            saveToStorage();
            return true;
        } else {
            return false;
        }

    }

    private void notifyItemChanged(Item item) {
        for (WeakReference<Listener> weakReference : listeners) {
            Listener listener = weakReference.get();
            if (listener != null) {
                listener.onItemChanged(item);
            }
        }
    }

    private void notifyItemAdded(Item item) {
        for (WeakReference<Listener> weakReference : listeners) {
            Listener listener = weakReference.get();
            if (listener != null) {
                listener.onItemAdded(item);
            }
        }
    }
}