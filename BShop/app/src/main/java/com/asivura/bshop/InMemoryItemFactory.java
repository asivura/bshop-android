package com.asivura.bshop;

import com.asivura.bshop.data.InMemoryItem;
import com.asivura.bshop.data.Item;
import com.asivura.bshop.data.ItemFactory;

/**
 * Created by asivura on 05.02.15.
 */
public class InMemoryItemFactory implements ItemFactory{
    @Override
    public Item createItem() {
        return new InMemoryItem();
    }
}
