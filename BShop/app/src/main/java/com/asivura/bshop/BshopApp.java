package com.asivura.bshop;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.asivura.bshop.data.Item;
import com.asivura.bshop.data.ItemService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.inject.Inject;

import dagger.Lazy;
import dagger.ObjectGraph;

/**
 * Created by asivura on 01.02.15.
 */
public class BshopApp extends Application {
    private ObjectGraph objectGraph;

    @Inject
    ItemService itemService;

    @Override
    public void onCreate() {
        super.onCreate();
        objectGraph = ObjectGraph.create(new BshopModule(this));
        objectGraph.inject(this);
    }

    public static BshopApp get(Context context) {
        return (BshopApp) context.getApplicationContext();
    }

    public void inject(Object o) {
        objectGraph.inject(o);
    }

}
