package com.asivura.bshop.ui;


import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.asivura.bshop.BshopApp;
import com.asivura.bshop.R;
import com.asivura.bshop.data.Item;
import com.asivura.bshop.data.ItemService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class StoreFrontFragment extends Fragment implements PagerFragment.Listener,
        PagerFragment.Adapter, ItemService.Listener, ViewPager.OnPageChangeListener {

    @Inject ItemService itemService;


    List<Item> items;
    Item selectedItem;

    public StoreFrontFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BshopApp.get(getActivity()).inject(this);
        itemService.addListener(this);
        reloadData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_store_front, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.content, new PagerFragment())
                .commit();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        itemService.removeListener(this);
    }

    @Override
    public void onAttach(PagerFragment pagerFragment) {
        pagerFragment.setAdapter(this);
    }

    @Override
    public void onDetach(PagerFragment pagerFragment) {
        pagerFragment.setAdapter(null);
    }

    @Override
    public Fragment getFragment(int position) {
        Item item = items.get(position);
        return ItemFragment.newInstance(item.getId());
    }

    @Override
    public int getInitialPosition() {
        if (selectedItem == null ) {
            return 0;
        }
        int index = items.indexOf(selectedItem);

        return index == -1?0:index;
    }

    @Override
    public int getPageCount() {
        return items.size();
    }

    @Override
    public ViewPager.OnPageChangeListener getOnPageChangeListener() {
        return this;
    }

    @Override
    public void onItemChanged(Item item) {
        reloadData();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.content, new PagerFragment())
                .commit();
    }

    @Override
    public void onItemAdded(Item item) {
        reloadData();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.content, new PagerFragment())
                .commit();
    }

    private void reloadData() {
        List<Item> allItems = itemService.getAll();
        ArrayList<Item> newItems = new ArrayList<>(allItems.size());
        for (Item item : allItems) {
            if (item.getQuantity() > 0) {
                newItems.add(item);
            }
        }
        items = newItems;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        this.selectedItem = items.get(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
