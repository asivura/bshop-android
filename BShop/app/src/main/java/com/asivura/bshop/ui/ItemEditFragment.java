package com.asivura.bshop.ui;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.asivura.bshop.BshopApp;
import com.asivura.bshop.R;
import com.asivura.bshop.data.Item;
import com.asivura.bshop.data.ItemService;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class ItemEditFragment extends Fragment {

    private static final String ARG_ITEM_ID = "item_id";

    @Inject
    ItemService itemService;

    Item item;

    @InjectView(R.id.title)
    EditText title;

    @InjectView(R.id.quantity)
    TextView quantity;

    @InjectView(R.id.add_items)
    EditText addQuantity;

    @InjectView(R.id.price)
    EditText price;

    @InjectView(R.id.add_items_label)
    TextView addQuantityLabel;

    public static ItemEditFragment newInstance(String itemId) {
        ItemEditFragment fragment = new ItemEditFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM_ID, itemId);
        fragment.setArguments(args);
        return fragment;
    }

    public ItemEditFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BshopApp.get(getActivity()).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_edit, container, false);
        ButterKnife.inject(this, view);

        if (getArguments() != null) {
            String itemId = getArguments().getString(ARG_ITEM_ID);
            if (itemId == null) {
                item = itemService.create();
                quantity.setFocusable(true);
                addQuantityLabel.setVisibility(View.GONE);
                addQuantity.setVisibility(View.GONE);
            } else {
                item = itemService.get(itemId);
            }
        }

        updateViews();
        return view;
    }

    private void updateViews() {
        title.setText(item.getTitle());
        quantity.setText(item.getQuantity().toString());
        price.setText(item.getPrice().toString());

    }

    @Override
    public void onStop() {
        super.onStop();
        item.setTitle(title.getText().toString())
                .setPrice(Double.valueOf(price.getText().toString()))
                .setQuantity(Integer.valueOf(quantity.getText().toString()));

        itemService.asyncSave(item, Integer.valueOf(addQuantity.getText().toString()),
                new ItemService.OperationListener() {
                    @Override
                    public void onCompleted(Context context) {
                        Toast.makeText(context,
                                R.string.changes_saved, Toast.LENGTH_SHORT)
                                .show();
                    }

                    @Override
                    public void onError(Context context, Exception e) {
                        Toast.makeText(context,
                                R.string.changes_saved_failed, Toast.LENGTH_SHORT)
                                .show();
                    }
                });

        Toast.makeText(getActivity(), R.string.operation_will_complete_in_3_seconds, Toast.LENGTH_SHORT)
                .show();
    }
}
