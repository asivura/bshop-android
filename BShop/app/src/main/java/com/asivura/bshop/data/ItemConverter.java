package com.asivura.bshop.data;

import java.util.List;

/**
 * Created by asivura on 05.02.15.
 */
public interface ItemConverter {

    public String toString(List<Item> itemList);

    public List<Item> fromString(String string);
}
