package com.asivura.bshop.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.asivura.bshop.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class PagerFragment extends Fragment {

    @InjectView(R.id.pager)
    ViewPager pager;

    Adapter adapter;
    Listener listener;

    public static interface Listener {
        public void onAttach(PagerFragment pagerFragment);

        public void onDetach(PagerFragment pagerFragment);
    }

    public static interface Adapter {
        public Fragment getFragment(int position);

        public int getInitialPosition();

        public int getPageCount();

        public ViewPager.OnPageChangeListener getOnPageChangeListener();
    }

    public PagerFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            if (getParentFragment() != null) {
                listener = (Listener) getParentFragment();
            } else {
                listener = (Listener) getActivity();
            }

        } catch (ClassCastException e) {
            throw new ClassCastException("Parent fragment or activity must implement PagerFragment.Listener");
        }

        listener.onAttach(this);
    }

    public void setAdapter(Adapter adapter) {
        this.adapter = adapter;
        initPager();
    }


    private void initPager() {
        if (adapter == null || pager == null) {
            return;
        }
        pager.setAdapter(new DetailFragmentStatePagerAdapter(getChildFragmentManager()));
        pager.setPageMargin((int) getResources().getDimension(R.dimen.page_margin));
        pager.setOffscreenPageLimit(1);
        pager.setCurrentItem(adapter.getInitialPosition());
        pager.setOnPageChangeListener(adapter.getOnPageChangeListener());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pager, container, false);
        ButterKnife.inject(this, view);
        initPager();
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (listener != null) {
            listener.onDetach(this);
        }
    }

    public class DetailFragmentStatePagerAdapter extends FragmentStatePagerAdapter {

        public DetailFragmentStatePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return adapter.getPageCount();
        }

        @Override
        public Fragment getItem(int position) {
            if (adapter == null) {
                throw new IllegalStateException("Adapter not set");
            }
            return adapter.getFragment(position);
        }

        @Override
        public String getPageTitle(int position) {
                return null;
        }
    }

}
