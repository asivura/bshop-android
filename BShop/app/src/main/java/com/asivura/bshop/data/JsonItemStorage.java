package com.asivura.bshop.data;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.asivura.bshop.R;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

/**
 * Created by asivura on 05.02.15.
 */
public class JsonItemStorage implements ItemStorage {
    private static final String FILE_NAME = "storage";
    private final Context context;
    private final ItemFactory itemFactory;
    private final ItemConverter itemConverter;

    public JsonItemStorage(Application app, ItemFactory itemFactory, ItemConverter itemConverter) {
        context = app;
        this.itemFactory = itemFactory;
        this.itemConverter = itemConverter;
    }

    @Override
    public void write(List<Item> items) throws IOException {
        FileOutputStream outputStream = null;

        try {
            outputStream = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            outputStream.write(itemConverter.toString(items).getBytes());
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }

    }

    @Override
    public List<Item> read() throws IOException {
        List<Item> items = new ArrayList<Item>();

        FileInputStream inputStream = null;
        try {
            inputStream = context.openFileInput(FILE_NAME);
            String content = convertStreamToString(inputStream);
            return itemConverter.fromString(content);
        } catch (FileNotFoundException e) {
            return readFromCSV();
        }
    }

    public static String convertStreamToString(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        return sb.toString();
    }


    private List<Item> readFromCSV() {
        ArrayList<Item> items = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources()
                    .openRawResource(R.raw.data)));
            String line = reader.readLine();
            while (line != null) {
                line = TextUtils.substring(line, 1, line.length() - 1);
                String[] values = TextUtils.split(line, "\", \"");
                Item item = itemFactory.createItem()
                        .setTitle(values[0])
                        .setPrice(Double.valueOf(values[1]))
                        .setQuantity(Integer.valueOf(values[2]))
                        .setId(UUID.randomUUID().toString());

                items.add(item);

                line = reader.readLine();
            }
            return items;
        } catch (IOException e) {
            throw new RuntimeException("Parse data.csv failed with error", e);
        }
    }
}
