package com.asivura.bshop.ui;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.asivura.bshop.BshopApp;
import com.asivura.bshop.R;
import com.asivura.bshop.data.Item;
import com.asivura.bshop.data.ItemService;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class ItemFragment extends Fragment {

    private static final String ARG_ITEM_ID = "item_id";

    @Inject ItemService itemService;
    @Inject BshopApp app;



    Item item;

    @InjectView(R.id.title)
    TextView title;

    @InjectView(R.id.quantity)
    TextView quantity;

    @InjectView(R.id.price)
    TextView price;

    public static ItemFragment newInstance(String itemId) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM_ID, itemId);
        fragment.setArguments(args);
        return fragment;
    }

    public ItemFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BshopApp.get(getActivity()).inject(this);
        if (getArguments() != null) {
            item = itemService.get(getArguments().getString(ARG_ITEM_ID));
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item, container, false);
        ButterKnife.inject(this, view);
        updateViews();
        return view;
    }

    private void updateViews() {
        title.setText(item.getTitle());
        quantity.setText(item.getQuantity().toString() + " " +
                getActivity().getString(R.string.pcs));
        price.setText(item.getPrice() + " " + getActivity().getString(R.string.currency));
    }

    @OnClick(R.id.buy)
    public void buyClicked() {
        itemService.asyncBuy(item, new ItemService.OperationListener() {
            @Override
            public void onCompleted(Context context) {
                Toast.makeText(context, R.string.purchase_completed, Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onError(Context context, Exception e) {
                Toast.makeText(context, R.string.purchase_failed, Toast.LENGTH_SHORT)
                        .show();

            }
        });
        Toast.makeText(getActivity(), R.string.operation_will_complete_in_5_seconds, Toast.LENGTH_SHORT)
                .show();

    }
}
