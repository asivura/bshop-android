package com.asivura.bshop.data;

import android.app.Application;

import com.asivura.bshop.InMemoryItemFactory;
import com.asivura.bshop.ui.BackendFragment;
import com.asivura.bshop.ui.ItemEditFragment;
import com.asivura.bshop.ui.ItemFragment;
import com.asivura.bshop.ui.StoreFrontFragment;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by asivura on 01.02.15.
 */

@Module(
        injects = {
                BackendFragment.class,
                ItemEditFragment.class,
                ItemFragment.class,
                StoreFrontFragment.class
        },
        complete = false,
        library = true
)
public class DataModule {

    @Provides
    @Singleton
    ItemFactory provideItemFactory() {
        return new InMemoryItemFactory();
    }

    @Provides
    @Singleton
    ItemConverter provideItemConverter(ItemFactory itemFactory) {
        return new JsonItemConverter(itemFactory);
    }

    @Provides
    @Singleton
    ItemStorage provideItemStorage(Application app, ItemFactory itemFactory, ItemConverter itemConverter) {
        return new JsonItemStorage(app, itemFactory, itemConverter);
    }

    @Provides
    @Singleton
    ItemService provideItemService(Application app, ItemStorage storage) {
        return new InMemoryItemService(app, storage);
    }
}
