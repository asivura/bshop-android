package com.asivura.bshop.data;

import android.content.Context;

import java.util.List;

/**
 * Created by asivura on 01.02.15.
 */
public interface ItemService {
    public List<Item> getAll();
    public Item get(String id);
    public void asyncSave(Item item, Integer quantityInc, OperationListener listener);
    public void asyncBuy(Item item, OperationListener listener);
    public Item create();
    public void addListener(Listener listener);
    public void removeListener(Listener listener);

    public interface Listener {
        public void onItemChanged(Item item);
        public void onItemAdded(Item item);
    }

    public interface OperationListener {
        public void onCompleted(Context context);
        public void onError(Context context, Exception e);
    }
}
