package com.asivura.bshop.data;

/**
 * Created by asivura on 31.01.15.
 */
public class InMemoryItem implements Item {
    String id;
    String title;
    double price;
    int quantity;

    public InMemoryItem() {

    }

    @Override
    public boolean equals(Object o) {
        //noinspection SimplifiableIfStatement
        if (o instanceof Item) {
            return getId().equals(((Item) o).getId());
        } else {
            return false;
        }
    }


    @Override
    public String getId() {
        return id;
    }

    @Override
    public Item setId(String id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Item setTitle(String title) {
        this.title = title;
        return this;
    }

    public Double getPrice() {
        return price;
    }

    public Item setPrice(Double price) {
        this.price = price;
        return this;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public Item setQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }


}
