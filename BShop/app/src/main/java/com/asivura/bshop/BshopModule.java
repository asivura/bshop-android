package com.asivura.bshop;

import android.app.Application;

import com.asivura.bshop.data.DataModule;
import com.asivura.bshop.data.ItemFactory;
import com.asivura.bshop.ui.ItemEditFragment;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by asivura on 01.02.15.
 */
@Module(
        injects = {BshopApp.class},
        includes = {DataModule.class}
)
public final class BshopModule {
    private final BshopApp app;

    public BshopModule(BshopApp app) {
        this.app = app;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return app;
    }

}

