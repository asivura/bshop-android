package com.asivura.bshop.data;

import java.io.IOException;
import java.util.List;

/**
 * Created by asivura on 05.02.15.
 */
public interface ItemStorage {

    public void write(List<Item> items) throws IOException;

    public List<Item> read() throws IOException;


}
