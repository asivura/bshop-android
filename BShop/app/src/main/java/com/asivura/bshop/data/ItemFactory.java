package com.asivura.bshop.data;

/**
 * Created by asivura on 05.02.15.
 */
public interface ItemFactory {

    public Item createItem();

}
