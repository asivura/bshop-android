package com.asivura.bshop.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by asivura on 05.02.15.
 */
public class JsonItemConverter implements ItemConverter {
    ItemFactory itemFactory;

    public JsonItemConverter(ItemFactory itemFactory) {
        this.itemFactory = itemFactory;
    }

    @Override
    public String toString(List<Item> items) {
        JSONArray jsonArray = new JSONArray();
        try {
            for (Item item : items) {
                jsonArray.put(new JSONObject()
                        .putOpt("id", item.getId())
                        .putOpt("title", item.getTitle())
                        .putOpt("price", item.getPrice())
                        .putOpt("quantity", item.getQuantity()));
            }
        } catch (JSONException exception) {
            throw new RuntimeException("Convert item failed", exception);
        }

        return jsonArray.toString();
    }

    @Override
    public List<Item> fromString(String string) {
        try {
            JSONArray jsonArray = new JSONArray(string);
            ArrayList<Item> items = new ArrayList<>(jsonArray.length());
            for (int i=0; i<jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                items.add(itemFactory.createItem()
                        .setId(jsonObject.optString("id"))
                .setTitle(jsonObject.optString("title"))
                .setPrice(jsonObject.optDouble("price"))
                .setQuantity(jsonObject.optInt("quantity")));
            }
            return  items;
        } catch (JSONException exception) {
            throw new RuntimeException("Convert item failed", exception);
        }
    }
}
