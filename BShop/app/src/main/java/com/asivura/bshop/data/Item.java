package com.asivura.bshop.data;

/**
 * Created by asivura on 01.02.15.
 */
public interface Item{

    public Item setId(String id);
    public String getId();

    public String getTitle();
    public Item setTitle(String title);

    public Double getPrice();
    public Item setPrice(Double price);

    public Integer getQuantity();
    public Item setQuantity(Integer quantity);



}
