package com.asivura.bshop.ui;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.asivura.bshop.BshopApp;
import com.asivura.bshop.R;
import com.asivura.bshop.data.Item;
import com.asivura.bshop.data.ItemService;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class BackendFragment extends ListFragment implements ItemService.Listener {

    @Inject ItemService itemService;

    List<Item> items;
    BaseAdapter adapter;

    public BackendFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BshopApp.get(getActivity()).inject(this);
        setHasOptionsMenu(true);
        itemService.addListener(this);
        items = itemService.getAll();
        adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return items.size();
            }

            @Override
            public Object getItem(int position) {
                return items.get(position);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View view, ViewGroup parent) {
                ViewHolder holder;

                if (view != null) {
                    holder = (ViewHolder) view.getTag();
                } else {
                    LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                    view = layoutInflater.inflate(R.layout.list_item, parent, false);
                    holder = new ViewHolder(view);
                    view.setTag(holder);
                }

                Item item = items.get(position);
                holder.title.setText(item.getTitle());
                holder.subtitle.setText(item.getQuantity().toString() + " " +
                        getActivity().getString(R.string.pcs));

                return view;
            }

        };

        setListAdapter(adapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        itemService.removeListener(this);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Item item = items.get(position);
        Intent intent = new Intent(getActivity(), ItemEditActivity.class);
        intent.putExtra(ItemEditActivity.EXTRA_ITEM_ID, item.getId());
        startActivity(intent);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(1);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.backend, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_item:
                Intent intent = new Intent(getActivity(), ItemEditActivity.class);
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onItemChanged(Item item) {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemAdded(Item item) {
        adapter.notifyDataSetChanged();
    }

    public static class ViewHolder {
        @InjectView(R.id.title)
        TextView title;
        @InjectView(R.id.subtitle)
        TextView subtitle;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
