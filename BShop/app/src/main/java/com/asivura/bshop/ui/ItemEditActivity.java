package com.asivura.bshop.ui;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import com.asivura.bshop.R;


public class ItemEditActivity extends ActionBarActivity {
    public static final String EXTRA_ITEM_ID = "item_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_edit);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, ItemEditFragment.newInstance(
                            getIntent().getStringExtra(EXTRA_ITEM_ID)))
                    .commit();
        }
    }
}